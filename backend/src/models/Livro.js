const mongoose = require('mongoose');

const LivroSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true
    },
    autor: {
        type: String,
        required: true,
    },
    editora: {
        type: String
    },
    genero: {
        type: String,
        required: true,
    },
    idioma: {
        type: String,
        required: true,
    },
    numero_paginas: {
        type: Number,
        required: true,
    },
    ano: {
        type: Number,
        required: true,
    },
    descricao: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Livro', LivroSchema);