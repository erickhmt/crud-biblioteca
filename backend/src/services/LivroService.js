const Livro = require('../models/Livro');

module.exports = {
    async ListAll() {
        const livroRecord = await Livro.find({});

        console.log('livroRecord: ', livroRecord);

        if (!livroRecord) {
            throw new Error('Livros não encontrados');
        }

        return livroRecord;
    },

    async GetById(id) {
        const livroRecord = await Livro.findById(id);

        console.log('livroRecord: ', livroRecord);

        if (!livroRecord) {
            throw new Error('Livro não encontrado');
        }

        return livroRecord;
    },

    async Register(body) {
        try {
            const livroRecord = await Livro.create(body);
            return livroRecord;
        } catch (err) {
            console.error(err);
            throw new Error('Falha ao Cadastrar livro', err);
        }
    },

    async Update(body) {
        try {
            const livroRecord = await Livro.updateOne(body);
            return livroRecord;
        } catch (err) {
            console.error(err);
            throw new Error('Falha ao Atualizar livro', err);
        }
    },

    async Delete(id) {
        try {
            const livroRecord = await Livro.findByIdAndDelete(id);
            return livroRecord;
        } catch (err) {
            console.error(err);
            throw new Error('Falha ao deletar livro', err);
        }
    }
};