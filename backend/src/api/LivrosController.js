const router = require('express').Router();
const LivroService = require('../services/LivroService');

router.get('/', async (req, res) => {
    try {
        const response = await LivroService.ListAll();
        return res.json(response);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

router.get('/:livroId', async (req, res) => {
    try {
        const response = await LivroService.GetById(req.params.livroId);
        return res.json(response);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const response = await LivroService.Register(req.body);
        return res.json(response);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

router.put('/', async (req, res) => {
    try {
        const response = await LivroService.Update(req.body);
        return res.json(response);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

router.delete('/:livroId', async (req, res) => {
    try {
        const response = await LivroService.Delete(req.params.livroId);
        return res.json(response);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

module.exports = router;