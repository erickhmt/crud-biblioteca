const router = require('express').Router();

router.use('/livros', require('./LivrosController'));

module.exports = router;