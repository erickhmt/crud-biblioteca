export const startLoading = () => ({
    type: "START_LOADING"
});

export const finishLoading = () => ({
    type: "FINISH_LOADING"
});

const INITIAL_STATE = {
    isLoading: false
};

export default function loading(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'START_LOADING':
        return {
            ...state,
            isLoading: true
        };
        case 'FINISH_LOADING':
        return {
            ...state,
            isLoading: false
        };
        default:
        return state;
    }
}