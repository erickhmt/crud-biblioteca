import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Loading from './components/Loading';
import Routes from './Routes';
import Navbar from './components/Navbar';

const browserHistory = createBrowserHistory();

function App() {
  return (
    <>
      <Loading/>
      <Router history={browserHistory}>
        <Navbar />
          <div className="container">
          <Routes />
          </div>
      </Router>
    </>
  );
}

export default App;
