
import axios from 'axios';
import { store } from '../index';
import { startLoading, finishLoading } from '../reducers/loading';

export const URL_API = 'http://localhost:3333/api/';

const api = axios.create({
    baseURL: URL_API
});

api.interceptors.request.use(config => {
    store.dispatch(startLoading());
    return config;
});

api.interceptors.response.use((res, err) => {
    store.dispatch(finishLoading());
    return res;
});

export default api;