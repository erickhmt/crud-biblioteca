import React, { useState, useEffect } from 'react';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { Link, useHistory } from 'react-router-dom';
import api from '../../services/api';
import './styles.css';
import ConfirmModal from '../../components/ConfirmModal';

function LivroList() {
    const history = useHistory();
    const [livros, setLivros] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [idLivroSelecionado, setIdLivroSelecionado] = useState(0);

    useEffect(() => {
        loadLivros();
    }, []);

    async function loadLivros() {
        try {
            const response = await api.get('livros');

            console.log(response.data);
            if(response.data) {
                setLivros(response.data);
            }
        } catch (err) {
            console.error(err);
        }
    }

    function editarLivro(livroId) {
        history.push(`form/${livroId}`);
    }

    function openModalDeletar(livroId) {
        setIdLivroSelecionado(livroId);
        setShowModal(true);
    }

    function fecharModalDeletar() {
        setShowModal(false);
    }

    async function deletarLivro() {
        try {
            const response = await api.delete(`livros/${idLivroSelecionado}`);
            console.log(response.data);
            fecharModalDeletar();
        } catch (err) {
            console.error('Erro ao deletar livro: ', err);
            fecharModalDeletar();
        }
    }

    return (
        <>
            <h1>Listagem de livros</h1>
            <div className="card-container">

                <Link to="form">
                    <button className="btn">Novo +</button>
                </Link>

                <table>
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Autor</th>
                            <th>Gênero</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {livros.map(livro => (
                            <tr key={livro._id}>
                                <td>{livro.titulo}</td>
                                <td>{livro.autor}</td>
                                <td>{livro.genero}</td>
                                <td style={{maxWidth: '50px', textAlign: 'center'}}>
                                    <button className="btn" title="Editar" onClick={() => editarLivro(livro._id)}>
                                        <FaEdit className="icon" />
                                    </button>
                                    <button className="btn btn-red" title="deletar" onClick={() => openModalDeletar(livro._id)}>
                                        <FaTrash className="icon" />
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <ConfirmModal mensagem={"Deseja realmente deletar este item?"} showModal={showModal} handleSuccess={deletarLivro} handleClose={fecharModalDeletar}/>
        </>
    );
}

export default LivroList;