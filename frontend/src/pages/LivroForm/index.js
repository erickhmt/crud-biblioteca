import React, { useState, useEffect } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import { Formik, Form as FormikForm, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import api from '../../services/api';

const validations = yup.object().shape({
    titulo: yup
        .string()
        .required('Campo obrigatório'),
    autor: yup
        .string()
        .min(6, 'senha deve possuir ao menos 6 caracteres')
        .required('Campo obrigatório'),
    editora: yup
        .string(),
    genero: yup
        .string()
        .required('Campo obrigatório'),
    idioma: yup
        .string()
        .required('Campo obrigatório'),
    numero_paginas: yup
        .number()
        .required('Campo obrigatório'),
    ano: yup
        .number()
        .required('Campo obrigatório'),
    descricao: yup
        .string()
        .required('Campo obrigatório'),
    
});

function LivroForm() {
    const [state, setState] = useState({
        titulo: '',
        autor: '',
        editora: '',
        genero: '',
        idioma: '',
        numero_paginas: 0,
        ano: new Date().getFullYear(),
        descricao: ''
    });
    const params = useParams();
    const history = useHistory();

    useEffect(() => {        
        async function loadPost() {
            try {
                const livroId = params.livroId;

                if(livroId) {
                    const response = await api.get(`livros/${livroId}`);

                    console.log(response.data);
                    if(response.data) {
                        setState(response.data);
                    }
                }
            } catch (err) {
                console.error(err);
            }
        }

        loadPost();
    }, []);

    async function handleSubmit(form) {
        console.log(form);

        try {
            if(form) {
                const response = await api.post('livros', form);
                console.log(response.data);
                history.push('/')
            }
        } catch (err) {
            console.error(err);
        }
    }

    const handleInputChange = name => event => {
        console.log(event.target.value)

        const changedState = { ...state, [name]: event.target.value };
        setState(changedState);
    };

    return (
        <>
            <h1>Formulário de cadastro</h1>

            <div className="card-container">
                <Formik
                    initialValues={state}
                    values={state}
                    onSubmit={handleSubmit}
                    validationSchema={validations}
                >
                    {props => (
                    <form className="form-container" onSubmit={props.handleSubmit}>
                        <div className="input-container">
                            <input name="titulo" className={props.errors.titulo ? 'input-error' : ''}
                                placeholder="Título do livro"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.titulo}
                                // onChange={handleInputChange('titulo')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="titulo" />
                            </span>
                        </div>
                        <div className="input-container">
                            <input name="autor" className={props.errors.autor ? 'input-error' : ''}
                                placeholder="Autor"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.autor} 
                                // onChange={handleInputChange('autor')} 
                                />
                            <span className="error-message">
                                <ErrorMessage name="autor" />
                            </span>
                        </div>
                        <div className="input-container">
                            <input name="editora" className={props.errors.editora ? 'input-error' : ''}
                                placeholder="Editora"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.editora}
                                // onChange={handleInputChange('editora')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="editora" />
                            </span>
                        </div>
                        <div className="input-container">
                            <input name="genero" className={props.errors.genero ? 'input-error' : ''}
                                placeholder="Genero"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.genero}
                                // onChange={handleInputChange('genero')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="genero" />
                            </span>
                        </div>
                        <div className="input-container">
                            <input name="idioma" className={props.errors.idioma ? 'input-error' : ''}
                                placeholder="Idioma"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.idioma}
                                // onChange={handleInputChange('idioma')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="idioma" />
                            </span>
                        </div>
                        <div className="input-container">
                            <input name="numero_paginas" type="number" className={props.errors.numero_paginas ? 'input-error' : ''}
                                placeholder="Número de páginas"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.numero_paginas}
                                // onChange={handleInputChange('numero_paginas')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="numero_paginas" />
                            </span>
                        </div>

                        <div className="input-container">
                            <textarea name="descricao" className={props.errors.descricao ? 'input-error' : ''}
                                placeholder="Descrição"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.name} 
                                // value={state.descricao} 
                                // onChange={handleInputChange('descricao')}
                                />
                            <span className="error-message">
                                <ErrorMessage name="descricao" />
                            </span>
                        </div>

                        <hr />
                        <div style={{textAlign: 'right'}}>
                            <button className="btn btn-red" onClick={() => history.push('/')}>Cancelar</button>
                            <button type="submit" className="btn">Salvar</button>
                        </div>
                    </form>
                    )}
                </Formik>
            </div>
        </>
    );
}

export default LivroForm;