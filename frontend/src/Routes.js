import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import LivroList from './pages/LivroList';
import LivroForm from './pages/LivroForm';

const Routes = () => {
    return (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' render={() => <Redirect to='/list' />} />
            <Route path='/list' exact component={LivroList} />
            <Route path='/form' exact component={LivroForm} />
            <Route path='/form/:livroId' exact component={LivroForm} />
            <Route path='*' component={() => <h1>Page not found</h1>} />
        </Switch>
    </BrowserRouter>
    );
};

export default Routes;