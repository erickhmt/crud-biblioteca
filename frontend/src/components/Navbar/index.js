import React from 'react';
import './styles.css';
import { Link } from 'react-router-dom';

function Navbar() {
    return (
        <nav className="nav">
            <Link to="/">
                <span className="nav-title">CRUD Biblioteca</span>
            </Link>
        </nav>

    );
}

export default Navbar;