import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { FadeLoader } from 'react-spinners';
import './styles.css'

function Loading() {
    const isLoading = useSelector(({ loading }) => loading.isLoading);
    const [classes, setClasses] = useState('');

    useEffect(() => {
        setClasses(isLoading ? 'loading-overlay-content' : 'loading-overlay-content hide');
    }, [isLoading])

    return (
        <>
            <div className={classes}>
                <div className="loading-wrapper">
                    <FadeLoader
                        size={150}
                        color={'#FFF'}
                        loading={true}
                    />
                </div>
            </div>
        </>
    );
}


export default Loading;