import React from 'react';
import './styles.css';

function ConfirmModal({mensagem, showModal, handleSuccess, handleClose}) {

    function confirm() {
        handleSuccess();
    }

    return (
        <>
            {showModal ? (
                <div className="modal-background">
                    <div className="modal-container">
                        <p className="modal-message">{mensagem}</p>
                        <hr />
                        <div className="btn-container-right">
                            <button className="btn" onClick={confirm}>Sim</button>
                            <button className="btn btn-red" onClick={handleClose}>Não</button>
                        </div>
                    </div>
                </div>
            ) : null}
        </>
    );
}

export default ConfirmModal;